variable "networking_state_organization" {}
variable "networking_state_workspace" {}

variable "aws_region" {}

variable "scope_name" { default = "" }
variable "tags" { default = {} }

variable "api_container_img_url" { default = "registry.gitlab.com/hexablocks/components/api-demo" }
variable "api_container_img_tag" { default = "main" }
variable "env_api_container_img_tag" { default = "" }

variable "api_ecs_target_port" { default = 3000 }
variable "api_svc_cpu" { default = 256 }
variable "api_svc_memory" { default = 512 }
variable "api_svc_desired_count" { default = 1 }
variable "api_svc_deployment_maximun_percent" { default = 200 }
variable "api_svc_assign_public_ip" { default = false }
variable "api_domain_acm_arn" {}

variable "api_alb_logs_bucket" { default = "" }
