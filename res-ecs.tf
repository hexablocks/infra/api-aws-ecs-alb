locals {
  final_api_container_img_tag = coalesce(var.env_api_container_img_tag, var.api_container_img_tag)
}

resource "aws_ecs_cluster" "api" {
  name = "${var.scope_name}-api"
}

resource "aws_ecs_task_definition" "api" {
  family                   = "${var.scope_name}-api"
  execution_role_arn       = aws_iam_role.api_exec.arn
  # task_role_arn            = aws_iam_role.api_task.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.api_svc_cpu
  memory                   = var.api_svc_memory
  container_definitions    = jsonencode([
    {
      name      = "api"
      image     = "${var.api_container_img_url}:${local.final_api_container_img_tag}"
      cpu       = var.api_svc_cpu
      memory    = var.api_svc_memory
      essential = true

      portMappings = [
        {
          containerPort = var.api_ecs_target_port
          hostPort      = var.api_ecs_target_port
        }
      ]

      logConfiguration = {
        logDriver = "awslogs"
        options   = {
          awslogs-group         = aws_cloudwatch_log_group.ecs_api.name
          awslogs-region        = data.aws_region.current.name
          awslogs-stream-prefix = var.api_container_img_tag
        }
      }
    }
  ])

  skip_destroy = true
}

resource "aws_ecs_service" "api" {
  name            = "${var.scope_name}-api"
  cluster         = aws_ecs_cluster.api.id
  task_definition = aws_ecs_task_definition.api.arn
    
  desired_count              = var.api_svc_desired_count
  deployment_maximum_percent = var.api_svc_deployment_maximun_percent
  force_new_deployment       = true

  capacity_provider_strategy {
    # base              = 1   # La base es independiente del ponderado
    capacity_provider = "FARGATE"
    weight            = 1
  }

  capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
    weight            = 2
  }

  network_configuration {
    security_groups  = [module.ecs_api_sg.security_group_id]
    subnets          = (var.api_svc_assign_public_ip ?
                        data.terraform_remote_state.networking.outputs.vpc_public_subnets :
                        data.terraform_remote_state.networking.outputs.vpc_private_subnets)
    assign_public_ip = var.api_svc_assign_public_ip
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.api.arn
    container_name   = "api"
    container_port   = var.api_ecs_target_port
  }

  deployment_circuit_breaker {
    enable = true
    rollback = true
  }

  lifecycle {
    ignore_changes = [deployment_maximum_percent]
  }

  depends_on = [aws_alb_listener.api, aws_iam_role_policy_attachment.api_exec_ecs]
}

resource "aws_cloudwatch_log_group" "ecs_api" {
  name              = "/aws/ecs/${aws_ecs_cluster.api.name}"
  retention_in_days = 30
}

module "ecs_api_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4"

  name        = "${var.scope_name}-api-ecs"
  description = "Access for api ecs"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id

  ingress_with_cidr_blocks = [
    {
      cidr_blocks = data.terraform_remote_state.networking.outputs.vpc_cidr_block
      from_port   = var.api_ecs_target_port
      to_port     = var.api_ecs_target_port
      protocol    = "tcp"
      description = "Allow port 3000 from whole vpc"
    }
  ]

  egress_rules = ["all-all"]
}
