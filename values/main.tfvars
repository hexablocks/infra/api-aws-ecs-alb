networking_state_organization = "Hexablocks"
networking_state_workspace    = "networking-aws-main"

aws_region = "us-east-1"
scope_name = "live-prod"

api_svc_desired_count    = 3
api_svc_assign_public_ip = true
api_domain_acm_arn       = "arn:aws:acm:us-east-1:713498694274:certificate/eb8d83a1-10cd-45b5-964a-516718741e5b"
