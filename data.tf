data "aws_region" "current" {}

data "terraform_remote_state" "networking" {
  backend = "remote"

  config = {
    organization = var.networking_state_organization
    workspaces = {
      name = var.networking_state_workspace
    }
  }
}
