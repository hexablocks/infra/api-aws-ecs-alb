resource "aws_alb" "api" {
  name            = "${var.scope_name}-api"
  subnets         = data.terraform_remote_state.networking.outputs.vpc_public_subnets
  security_groups = [module.api_alb_sg.security_group_id]

  access_logs {
    enabled = alltrue([var.api_alb_logs_bucket != ""])
    bucket  = var.api_alb_logs_bucket
    prefix  = "${var.scope_name}/api-alb"
  }
}

resource "aws_alb_target_group" "api" {
  name        = "${var.scope_name}-api"
  port        = var.api_ecs_target_port
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 29
    protocol            = "HTTP"
    matcher             = "200"
    path                = "/predefined/200"
    interval            = 30
  }
}

resource "aws_alb_listener" "api" {
  load_balancer_arn = aws_alb.api.id
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.api_domain_acm_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.api.arn
  }
}

module "api_alb_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "4.13.1"

  name        = "${var.scope_name}-api-alb-sg"
  description = "Application load balancer access"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id

  ingress_with_cidr_blocks = [
    {
      cidr_blocks = "0.0.0.0/0"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "HTTPS Access"
    }
  ]

  egress_with_cidr_blocks = [
    {
      cidr_blocks = data.terraform_remote_state.networking.outputs.vpc_cidr_block
      from_port   = var.api_ecs_target_port
      to_port     = var.api_ecs_target_port
      protocol    = "tcp"
      description = "Allow access to all vpc"
    }
  ]
}